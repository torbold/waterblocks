# Water Blocks
Anyone is free to upload these mod files to steam, or change them in any way. Just credit Torbold in the mod's description, if you do use the code.

**Important blueprints**
 - Base_WaterBlock <br>&ensp;&ensp;**-** *Core functionality, the actual structure. Each water block is a child of this.* 
 - Buff_WB_Player_PostProcess <br>&ensp;&ensp;**-** *Controlls the water postprocess effects, as well as water particles.* 
 - Buff_WB_WaterDino <br>&ensp;&ensp;**-** *Buff that prevents water dinos from dieing on game restarts.* 
 - Buff_WaterBucketInventory <br>&ensp;&ensp;**-** *The inventory for the water bucket.* 
 - WaterBucket <br>&ensp;&ensp;**-** *Item to get water blocks.* 

**Adding to your dev kit**

 1. Download the Water Block files by clicking the cloud dropdown, then selecting **Download zip**.
 2. Unzip the download using the program of your choice.
 3. Copy the **TestMap** folder, found in: <br> ...\waterblocks-master\Maps\ <br>into your dev kit maps folder, found in: <br> ...\ARKDevKit\Projects\ShooterGame\Content\Maps\
 4. Copy the contents of the **TheIslandSubMaps** folder from: <br> ...\waterblocks-master\Maps\ <br>into the **TheIslandSubMaps** folder that is found in your dev kit maps folder: <br> ...\ARKDevKit\Projects\ShooterGame\Content\Maps\TheIslandSubMaps
 5. Copy the **WaterBlocks** folder: <br>...\waterblocks-master\Mods\WaterBlocks <br>into the **Mods** folder in the dev kit: <br>...\ARKDevKit\Projects\ShooterGame\Content\Mods\
 6. Done! Now just start the dev kit.

  
**Testing in the dev kit**

 - With the dev kit open, copy the **WB_1x1** through **WB_1x12** levels into the **TestMap** folder, found in: <br>\Content\Maps\
 - Open the **TestMap_DayCycle_Weather** map that is located in the **TestMap** folder.
 - Set **PrimalGameData_BP_WaterBlocks** as the Primal Game Data Override in the world settings window.
 - Play and test the mod in PIE.

**Testing in Ark**
 - Make sure the **WB_1x1** through **WB_1x12** levels are in the **TheIslandSubMaps** folder.
 - Click Steam Upload.
 - Click the WaterBlocks mod on the left.
 - Make sure that the the proper levels are in the Maps section:<br>WaterBlocks,WB_1x1,WB_1x2,WB_1x3,WB_1x4,WB_1x5,WB_1x6,WB_1x7,WB_1x8,WB_1x9,WB_1x10,WB_1x11,WB_1x12
 - Choose **Cook Island Extension**.
 - Fill out the mod upload information and upload the mod to steam.
 - Subscribe to the mod and play in game!

**Current known issues**
These are the current known issues with the mod, but I don't plan on fixing them anytime soon as this was just meant to be a proof of concept as well as a possible teaching tool for others to learn how to create level instances.

 - On dedicated servers, when the client reconnects near existing water blocks. The sides of the blocks are all visible when they should be hidden. Un-rendering and then re-rendering the affected water blocks resolves the issue.